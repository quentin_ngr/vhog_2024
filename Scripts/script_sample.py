from collections import defaultdict
import math


number_phrogs=3000
number_target=9900
number_proteins_selected=0

list_files = list()
dict_prot2seq = defaultdict(str)


with open("list_phrogs_selected.txt", "r") as f1:
        for li in f1:
                li = li.rstrip()
                list_files.append(li)

file_faa = open("sample_proteins.faa", "w")


nb_prot=number_proteins_selected/3000
debug=0
while number_proteins_selected < number_target:
	for i in range(len(list_files)):
		nb_seq=0
		with open(list_files[i%3000], "r") as f1:
			for li in f1:
				li=li.rstrip()
				if li[0] == ">":
					nb_seq +=1
					seqid = li
				else:
					sequence=li
					if nb_seq==math.ceil((number_proteins_selected+debug)/3000+0.000000000001):
						if seqid not in dict_prot2seq.keys():
							dict_prot2seq[seqid]=sequence
							number_proteins_selected+=1
							print(nb_seq, seqid, number_proteins_selected)
							print(seqid+"\n"+sequence, file=file_faa)
						else:
							debug+=1
		if number_proteins_selected >= number_target:
			break
print(number_proteins_selected)


