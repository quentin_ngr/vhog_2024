from collections import defaultdict
import sys
import math
import plotly.graph_objects as go
import os

filehmm=sys.argv[1]
dict_neff=defaultdict(float)
with open(filehmm, "r") as f1:
	for li in f1:
		li=li.rstrip()
		if li.startswith("NAME  "):
			name=li.split(" ")[-1]
		if li.startswith("NEFF "):
			neff=li.split("  ")[-1]
			#random_float=random.randint(0,9)/100
			dict_neff[name]=float(neff) #+random_float

#print(dict_neff)


#file = sys.argv[1]
dir=sys.argv[2]
files=os.listdir(dir)


dict_shannon_total=defaultdict(float)
dict_shannon_mean=defaultdict(float)


dict_infos=defaultdict(list)
#list_neff=[]
fig = go.Figure()
fig2 = go.Figure()
for file in files:
	file=dir+file
	print(file)
	dict_shannon=defaultdict(float)
	dict_sequences=defaultdict(str)
	first_seq=""
	with open(file, "r") as f1:
		for li in f1:
			li=li.rstrip()
			if first_seq=="":
				first_seq=li[1:].split(" ")[0]
#				name=first_seq
			if li.startswith(">"):
				name=li[1:].split(" ")[0]
				#print(name)
				dict_sequences[name]=""
			else:
				dict_sequences[name]+=li

	#print(dict_sequences)

	dict_pos=defaultdict(list)
	i=0 # place dans le MSA M 50, 1 - len-1
	j=0 # place dans le MSA, 1 - len
	for prot in dict_sequences:
		print(prot)
		print(first_seq)
		i=0
		j=0
		if prot == first_seq:
			list_match=[]
			for aa in dict_sequences[first_seq]:
				if aa != "-":
					i+=1
					dict_pos[str(i)].append(aa)
					list_match.append(j)
				j+=1
		else:
			for aa in dict_sequences[prot]:
				if aa.isupper() and j in list_match:
					i+=1
					if aa!="-":
						dict_pos[str(i)].append(aa)
				j+=1
	#print(dict_pos)

	dict_frequency=defaultdict(dict)
	for pos in dict_pos:
		for aa in dict_pos[pos]:
			if aa not in dict_frequency[pos].keys():
				dict_frequency[pos][aa]=0
			dict_frequency[pos][aa]+=1/len(dict_pos[pos])
		for aa in dict_frequency[pos]:
			dict_shannon[pos]+=(-1)*(dict_frequency[pos][aa]*math.log2(dict_frequency[pos][aa]))

	dict_shannon_total=defaultdict(float)
	dict_shannon_mean=defaultdict(float)
	for pos in dict_pos:
		#print(pos, dict_pos[pos], dict_frequency[pos], dict_shannon[pos])
		dict_shannon_total[file]+=dict_shannon[pos]
		dict_shannon_mean[file]+=dict_shannon[pos]/len(dict_pos)
	#print(dict_shannon)
#	print(dict_shannon_total)
#	print(dict_shannon_mean)
	name_title=file.split(".")[0].split("/")[-1]
	print(name_title)
	neff=str(dict_neff[name_title])
	print(neff)
	#list_neff.append(neff)
	dict_infos[name_title].append(neff)
	dict_infos[name_title].append(dict_shannon_mean[file])
	dict_infos[name_title].append(dict_shannon_total[file])
	fig.add_trace(go.Scatter(x=list(dict_shannon.keys()), y=list(dict_shannon.values()), mode="lines", name=name_title+" "+neff))


dict_infos=dict(sorted(dict_infos.items(), key=lambda item: float(item[1][0])))

fig2 = go.Figure(go.Scatter(x=list(float(value[0]) for value in dict_infos.values()), y=list(value[1] for value in dict_infos.values()), mode="markers", opacity=0.5, text=list(dict_infos.keys())))
fig3 = go.Figure(go.Scatter(x=list(float(value[0]) for value in dict_infos.values()), y=list(value[2] for value in dict_infos.values()), mode="markers", opacity=0.5, text=list(dict_infos.keys())))

fig.update_layout(
    title="Shannon value per position per MSA",
    xaxis_title="Position",
    yaxis_title="Shannon_index"
)

fig2.update_layout(
    title="Mean shannon index per Neff per MSA",
    xaxis_title="Neff",
    yaxis_title="Shannon_index",
    hovermode='closest'
)

fig3.update_layout(
    title="Total shannon index per Neff per MSA",
    xaxis_title="Neff",
    yaxis_title="Shannon_index",
    hovermode='closest'
)


print(dict_shannon_total)
print(dict_shannon_mean)

fig.write_html("shannon_OG33.html")
#fig2.write_html("neff_shannon_OG33.html")
#fig3.write_html("total_neff_shannon_OG33.html")
