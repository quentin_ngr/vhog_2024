from collections import defaultdict
import argparse 
import os
import numpy as np
import plotly.graph_objs as go

parser = argparse.ArgumentParser(description='Extract and make cumulative curves')
parser.add_argument('--input_files',nargs='+', help='All cluster files')
parser.add_argument('--names',nargs='+', help='Names of curves')
parser.add_argument('--types',nargs='+', help='mmseqs (mm) or mcl (mcl) type file')

args = parser.parse_args()

for i in args.input_files:
	print(i)

list_names=args.names
list_types=args.types
data=list()
count_curves=0
type_i=0

for file in args.input_files:
	count=0
	total_prot=0
	if list_types[type_i]=="mcl":
		dictCluster=defaultdict(list)
		with open(file, "r") as f1:
			for li in f1:
				count+=1
				li=li.rstrip()
				li=li.split("\t")
				total_prot+=len(li)
				dictCluster["cluster_"+str(count)]=li
	else:
		dictCluster=defaultdict(set)
		previous_consensus_proteins=""
		with open(file, "r") as f1:
			for li in f1:
				total_prot+=1
				li=li.rstrip()
				li=li.split("\t")
				if li[0] != previous_consensus_proteins:
					count+=1
					previous_consensus_proteins=li[0]
					#dictCluster["cluster_"+str(count)].add(li[0])
					dictCluster["cluster_"+str(count)].add(li[1])
				else:
					dictCluster["cluster_"+str(count)].add(li[1])
	#print(dictCluster)
	type_i+=1
	dict_lenCluster=defaultdict(int)
	for cluster in dictCluster:
		dict_lenCluster[str(len(dictCluster[cluster]))]+=1
	dict_lenCluster_sorted=dict(sorted(dict_lenCluster.items(), key=lambda x: int(x[0]), reverse = True)) #, key=lambda item: item[1]))
	print(dict_lenCluster_sorted)
	dict_cumulative=defaultdict(int)
	#dict_cumulative['1']=total_prot
	#for length in dict_lenCluster_sorted:
	#	dict_cumulative[
	#print(list(dict_lenCluster_sorted.keys()))
	dict_cumulative[list(dict_lenCluster_sorted.keys())[0]]=list(dict_lenCluster_sorted.keys())[0]*list(dict_lenCluster_sorted.values())[0]
	for i in range(int(list(dict_lenCluster_sorted.keys())[0])-1, 0, -1):
		if str(i) in dict_lenCluster_sorted.keys():
			#dict_cumulative[str(i)]+=dict_lenCluster_sorted[str(i)]+dict_cumulative[str(i+1)]
			dict_cumulative[str(i)]+=(i*dict_lenCluster_sorted[str(i)])+int(dict_cumulative[str(i+1)])
		else:
			dict_cumulative[str(i)]=dict_cumulative[str(i+1)]
	print(dict_cumulative)
	name_trace="trace"+str(count_curves)
	x=list(dict_cumulative.keys())
	y=list(dict_cumulative.values())
	print(list(reversed(x)), list(reversed(y)))
	name_trace = go.Scatter(
        x=list(reversed(x)),  # x-axis values
        y=list(reversed(y)),  # y-axis values
        mode='lines',       # type of plot (lines with markers)
        name=list_names[count_curves]
        )
	data.append(name_trace)
	count_curves+=1

layout = go.Layout(
        title='Cumulative curve',
        xaxis=dict(title='cluster size'),
        yaxis=dict(title='number of proteins')
)

fig = go.Figure(data=data, layout=layout)

fig.write_image("line_cumulative.svg")

