# Author : Quentin Nugier

# Main pipeline to clusterise proteins using the vhog approach

# Starting faa file of proteins : all_proteins.faa

############################################################
######### #Step one : mmseqs on itself, clustering, one faa per cluster, alignment
############################################################

mkdir tmp
mmseqs easy-cluster all_proteins.faa clusters_mmseqs_cov80 tmp --add-self-matches -a -s 7.5 --max-seqs 10000 -c 0.8
python3 multimsa_to_singlemsa.py clusters_mmseqs_cov80_all_seqs.fasta clusters_mmseqs_fasta faa

#no more than 50 000 files in a directory, so if > 50 000 : splitting into multiple dir (we'll call "batch" : each having a letter for name)

############################################################
######### Step two : building alignements (on the hpc)
############################################################

#alignements.slurm
#USE 'alignements.slurm a' to treat the directory of batch "a"
#no need to treat directories that contains only singletons

#alignements.slurm a
#alignements.slurm b
#alignements.slurm c

############################################################
######### Step three : building databases and hmms and enrich these (on the hpc)  
############################################################


#msa_to_enrich.sh on HPC

#USE : 'msa_to_enrich.sh a' to treat the directory of batch "a" 

#msa_to_enrich.sh a
#...
#msa_to_enrich.sh g

############################################################
######### Step three : building database of enriched hmms  
############################################################


mkdir hhblits_db_mmseqs
cd hhblits_db_mmseqs

# for each enriched alignment file (example with batch "a"):
ffindex_build -as enriched_mmseqs_a_unfiltered_a3m.ff{data,index}
ffindex_apply enriched_mmseqs_a_unfiltered_a3m.ff{data,index} -i enriched_mmseqs_a_a3m.ffindex -d enriched_mmseqs_a_a3m.ffdata -- hhfilter -i stdin -o stdout -v 0 -M a2m -neff 10 -diff inf -id 99
ffindex_apply enriched_mmseqs_a_a3m.ff{data,index} -i enriched_mmseqs_a_hhm.ffindex -d enriched_mmseqs_a_hhm.ffdata -- hhmake -i stdin -o stdout -v 0 -diff inf -id 95

#concatenate all the files : 
touch enriched_mmseqs_a3m.ffdata
touch enriched_mmseqs_a3m.ffindex
touch enriched_mmseqs_hhm.ffdata
touch enriched_mmseqs_hhm.ffindex

##for each batch:
ffindex_build -as enriched_mmseqs_a3m.ff{data,index} -d enriched_mmseqs_a_a3m.ffdata -i enriched_mmseqs_a_a3m.ffindex
ffindex_build -as enriched_mmseqs_hhm.ff{data,index} -d enriched_mmseqs_a_hhm.ffdata -i enriched_mmseqs_a_hhm.ffindex

#then, build cs219 file:
cstranslate -i enriched_mmseqs_a3m -o enriched_mmseqs_cs219 -b -f -I a3m -x 0.3 -c 4


############################################################
######### Step four : Compare each batch of enriched hmm with the enriched database (batches are better for HPC utilization)
############################################################

# hhblits.slurm a

#all results are in a blasttab format and detailed results in hhr format

############################################################
######### Step five : cleaning the results using hhr files (bad coverage...)
############################################################

# for each hhr file, it is parsed and a short clean file is created
for file in enriched_*_hhr.ffdata ; do echo $file ; python ../parse_hhr.py $file > ../hhr_treated/${file}.treated ; echo "done" ; done
cat ../hhr_treated/* > cat_hhr_treated

############################################################
######### Step seven : clusterization 
############################################################

cut -f 1,2,6 cat_hhr_treated > blast_parsed.abc
mcxload -abc blast_parsed.abc --stream-mirror -o blast_parsed.mci -write-tab blast_parsed.tab
mcl blast_parsed.mci -I 2 -use-tab blast_parsed.tab
mv out.blast_parsed.mci.I20 cluster_regrouped_cleaned.tsv

############################################################
######### Step eight : degroup clusters and rename the final vhogs
############################################################

#prot2clusters.tsv : tab file : col 1 = name prot ; col 2 : name mmseqs cluster it belongs to
regroup_clusters.py cluster_regrouped_cleaned.tsv > recap_clusters_clean.tsv


