import sys

filehhr=sys.argv[1]
flag=False
set_one=set()
set_all=set()

with open(filehhr, "r") as f1:
	for li in f1:
		li=li.rstrip()
		if li.startswith("Query") or li.startswith("\x00"):
			#print(li)
			if "         " in li:
				name_query=li.split("         ")[1]
				set_all.add(name_query)
		if li.startswith("Match_columns"):
			#print(li)
			lHMMquery=int(li.split(" ")[1])
		if li.startswith(" No Hit"):
			flag=True
		if li.startswith("No 1") or li=="":
			flag=False
		if flag==True and not li.startswith(" No Hit") and li!='':
			lis=li.replace("(", " ")
			lis=lis.split(" ")
			lis=list(filter(lambda a: a != '', lis))
			#print(lHMMquery, lis[7])
			#print(lis)
			name_target=lis[1]
			proba=lis[2]
			pval=lis[4]
			lAlign=int(lis[7])
			lHMMtarget=int(lis[10][:-1])
			#print(lAlign,lHMMtarget)
			#if lAlign>lHMMtarget*0.6:
			#	print(li)
			if name_query != name_target:
				if lAlign>=lHMMquery*0.6 and lAlign>=lHMMtarget*0.6:
					#print(li)
					print(name_query+"\t"+name_target+"\t"+str(lAlign)+"\t"+str(lHMMquery)+"\t"+str(lHMMtarget)+"\t"+proba+"\t"+pval)
			else:
				lAln=min(lHMMquery, lHMMtarget)
				print(name_query+"\t"+name_target+"\t"+str(lAln)+"\t"+str(lHMMquery)+"\t"+str(lHMMtarget)+"\t"+"100"+"\t"+"1e-200")
				set_one.add(name_query)
#print(set_one)
#print(len(set_all))
#print(len(set_one))
for prot in set_all:
	if prot not in set_one:
		print(prot+"\t"+prot+"\t"+"?"+"\t"+"?"+"\t"+"?"+"\t"+"100"+"\t"+"1e-200")
