# script to extract and analyse NEFFs
import plotly.graph_objects as go
import argparse
from collections import defaultdict
import numpy as np

parser = argparse.ArgumentParser(description='Compare Neff of two differents hhr files')
parser.add_argument('--fileA', type=str, default='/home/qunugier/Clusterisation/test2/preclusterisation/clust_by_mmseqs/hhblits_db/hhblits_RepseqvsAll/enriched_repseq_hhr.ffdata', help='Path to the file of hhr A')
parser.add_argument('--fileB', type=str, help='Path to the file of hhr B')
parser.add_argument('-m', '--means', action='store_true', help='With means')

#parser.add_argument('outdir', type=str, help='Directory to write clusters')
#parser.add_argument('threshold', type=int, help='How many cluster B there is in cluster A to deem it interesting')
args = parser.parse_args()


#file_hhr1="/home/qunugier/Clusterisation/test2/preclusterisation/clust_by_mmseqs/hhblits_db/hhblits_RepseqvsAll/p90/enriched_repseq_hhr.ffdata"
file_hhr1=args.fileA
dict_neff1=dict()
list_neff1=list()
with open(file_hhr1, "r") as f1:
	for li in f1:
		if li.startswith("\x00Query") or li.startswith("Query"):
			name=li.rstrip().split(" ")[-1]
			print(name)
		if li.startswith("Neff"):
			neff=li.rstrip().split(" ")[-1]
			list_neff1.append(float(neff))
			dict_neff1[name]=float(neff)



dict_neff1_sorted = dict(sorted(dict_neff1.items(), key=lambda item: item[1], reverse=True))
print(dict_neff1_sorted)


dict_cov_per_prot=defaultdict(float)
dict_depth_per_prot=defaultdict(float)
dict_cov_per_position=defaultdict(float)
with open("infos.txt", "r") as f3:
	for li in f3:
		li=li.rstrip()
		li=li.split("\t")
		if li[0]=="coverage":
			dict_cov_per_prot[li[1]]=float(li[2])
		#elif li[0]=="depth":
		#elif li[0]=="pos_cov":

dict_cov_per_prot_sorted={k: dict_cov_per_prot[k] for k in dict_neff1_sorted}



list(range(len(list_neff1)))

liste_neff1_sorted=sorted(list_neff1, reverse=True)
trace1 = go.Scatter(
        x=list(dict_neff1_sorted.keys()),  # x-axis values
        y=list(dict_neff1_sorted.values()),                    # y-axis values
        mode='lines',       # type of plot (lines with markers)
        name="FileA, cov 0.7, Neff"
	)

trace3 = go.Scatter(
        x=list(dict_neff1_sorted.keys()),  # x-axis values
        y=list(dict_cov_per_prot_sorted.values()),                    # y-axis values
        mode='markers',       # type of plot (lines with markers)
        name="FileA, cov 0.7, coverage",
	marker=dict(color='blue', size=3)
        )


data=[trace1, trace3]

boxplot1 = go.Box(
	y=list(dict_cov_per_prot_sorted.values()),
	name="FileA, cov0.7",
	marker=dict(color="blue")
	)

data_boxplots=[boxplot1]

'''
x = np.array(list(range(len(dict_cov_per_prot_sorted.keys()))))
y = np.array(list(dict_cov_per_prot_sorted.values()))

y_def = y[y!=0]

print(len(y_def))

slope, intercept = np.polyfit(x[:len(y_def)], y_def, 1)
y_pred = slope * x[:len(y_def)] + intercept

trace4 = go.Scatter(
        x=list(dict_neff1_sorted.keys())[:len(y_def)],  # x-axis values
        y=y_pred,                    # y-axis values
        mode='lines',       # type of plot (lines with markers)
        name="Droite de régression FileA",
        marker=dict(color='green')
        )

data=[trace1, trace3, trace4]
'''
if args.fileB:
	#file_hhr2="/home/qunugier/Clusterisation/test/preclusterisation/clust_by_mmseqs/hhblits_db/hhblits_RepseqvsAll/enriched_repseq_hhr.ffdata"
	file_hhr2=args.fileB
	list_neff2=list()
	dict_neff2=dict()
	with open(file_hhr2, "r") as f2:
		for li in f2:
			if li.startswith("\x00Query") or li.startswith("Query"):
				name=li.rstrip().split(" ")[-1]
			#	print(name)
			if li.startswith("Neff"):
				neff=li.rstrip().split(" ")[-1]
				list_neff2.append(float(neff))
				dict_neff2[name]=float(neff)
	dict_neff2_sorted = {k: dict_neff2[k] for k in dict_neff1_sorted}
	liste_neff2_sorted=sorted(list_neff2, reverse=True)
	trace2 = go.Scatter(
		x=list(dict_neff1_sorted.keys()),  # x-axis values
		y=list(dict_neff2_sorted.values()),                    # y-axis values
		mode='lines',       # type of plot (lines with markers)
		name="FileB",
		line=dict(color="red")
		)
	data.append(trace2)

	dict_cov_per_prot=defaultdict(float)
	dict_depth_per_prot=defaultdict(float)
	dict_cov_per_position=defaultdict(float)
	with open("cov0/infos.txt", "r") as f3:
		for li in f3:
			li=li.rstrip()
			li=li.split("\t")
			if li[0]=="coverage":
				dict_cov_per_prot[li[1]]=float(li[2])
	dict_cov_per_prot_sorted={k: dict_cov_per_prot[k] for k in dict_neff1_sorted}
	trace5 = go.Scatter(
		x=list(dict_neff1_sorted.keys()),  # x-axis values
		y=list(dict_cov_per_prot_sorted.values()),                    # y-axis values
		mode='markers',       # type of plot (lines with markers)
		name="FileB, cov 0, coverage",
		marker=dict(color='red', size=3)
		)
	data.append(trace5)	

	boxplot2 = go.Box(
		y=list(dict_cov_per_prot_sorted.values()),
		name="FileB, cov0",
		marker=dict(color="red")
		)
	data_boxplots.append(boxplot2)

if args.means:
# Create a horizontal line at y=4
	moyenne_neff1=sum(list_neff1)/len(list_neff1)
	print(moyenne_neff1)
	mean1 = go.Scatter(
    		x=list(dict_neff1_sorted.keys()),  # x-axis values for the line
    		y=[moyenne_neff1] * len(liste_neff1_sorted),          # y-axis values for the line (constant value 4)
    		mode='lines',
    		line=dict(color='blue', width=0.8, dash='dash'),      # color of the line
    		name='Mean1'       # name of the trace
		)
	data.append(mean1)
	if args.fileB:
		# Create a horizontal line at y=4
		moyenne_neff2=sum(list_neff2)/len(list_neff2)
		print(moyenne_neff2)
		mean2 = go.Scatter(
    			x=list(dict_neff1_sorted.keys()),  # x-axis values for the line
    			y=[moyenne_neff2] * len(liste_neff2_sorted),          # y-axis values for the line (constant value 4)
    			mode='lines',
    			line=dict(color='red', width=0.8, dash='dash'),      # color of the line
    			name='Mean2'       # name of the trace
			)
		data.append(mean2)
else:
	data = data

layout = go.Layout(
	title='Line Plot with Plotly',
	xaxis=dict(title='X-axis'),
	yaxis=dict(title='Y-axis')
)

fig = go.Figure(data=data, layout=layout)

fig.write_html("line_plot.html")

layout = go.Layout(
        title='Boxplot of coverage',
        xaxis=dict(title='-cov value'),
        yaxis=dict(title='coverage')
)

fig = go.Figure(data=data_boxplots, layout=layout)

fig.write_html("boxplot.html")

